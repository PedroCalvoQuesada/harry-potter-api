/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */
'use strict';

const app = document.getElementById('app');

function createDOM() {
  createHeader();
  setTimeout(()=> {
    createMenu();
    createMainSection();
    createEvents();
  }, 2000);
}

function createHeader() {
  const header = document.createElement('header');
  const title = document.createElement('h1');
  const titleText = document.createTextNode('Harry Potter API');

  header.classList.add('header');
  title.classList.add('title');

  title.appendChild(titleText);
  header.appendChild(title);
  app.appendChild(header);
}

function createMenu() {
  const menu = document.createElement('nav');
  const menuItem1 = document.createElement('a');
  const menuItem2 = document.createElement('a');
  const menuItem3 = document.createElement('a');
  const menuItem4 = document.createElement('a');
  const menuItemText1 = document.createTextNode('Magos y Brujas');
  const menuItemText2 = document.createTextNode('Bestiario');
  const menuItemText3 = document.createTextNode('Hechizos y Encantamientos');
  const menuItemText4 = document.createTextNode('Casas de Hogwarts');

  menu.classList.add('menu');

  menuItem1.setAttribute('id', 'wizards');
  menuItem2.setAttribute('id', 'beasts');
  menuItem3.setAttribute('id', 'spells');
  menuItem4.setAttribute('id', 'houses');

  menuItem1.classList.add('menu__item');
  menuItem2.classList.add('menu__item');
  menuItem3.classList.add('menu__item');
  menuItem4.classList.add('menu__item');

  menuItem1.setAttribute('href', '#');
  menuItem2.setAttribute('href', '#');
  menuItem3.setAttribute('href', '#');
  menuItem4.setAttribute('href', '#');

  menuItem1.appendChild(menuItemText1);
  menuItem2.appendChild(menuItemText2);
  menuItem3.appendChild(menuItemText3);
  menuItem4.appendChild(menuItemText4);

  menu.appendChild(menuItem1);
  menu.appendChild(menuItem2);
  menu.appendChild(menuItem3);
  menu.appendChild(menuItem4);

  app.appendChild(menu);
}

function createMainSection() {
  const mainSection = document.createElement('section');

  mainSection.setAttribute('id', 'main-section');
  mainSection.classList.add('main-section');

  app.appendChild(mainSection);
}

function createEvents() {
  const wizardSelection = document.getElementById('wizards');
  const bestiarySelection = document.getElementById('beasts');

  wizardSelection.addEventListener('click', ()=> showWizards());
  bestiarySelection.addEventListener('click', ()=> showBestiary());
}
