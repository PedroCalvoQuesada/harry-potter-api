/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */
'use strict';

function showWizards() {
  const mainSection = document.getElementById('main-section');
  mainSection.innerHTML = '';

  const gallery = document.createElement('div');

  gallery.classList.add('gallery');

  fetch('http://localhost:3000/wizards')
      .then((response) => response.json())
      .then((wizards) => {
        for (const wizard of wizards) {
          createWizardCard(wizard.name, wizard.bio, wizard.img);
        }
      });

  mainSection.appendChild(gallery);

  changeBSO('Harry_Potter_bso.mp3');
}

function createWizardCard(name, bio, img) {
  const gallery = document.querySelector('.gallery');
  const card = document.createElement('div');
  const cardInner = document.createElement('div');
  const cardFront = document.createElement('div');
  const cardBack = document.createElement('div');
  const cardImg = document.createElement('img');
  const cardName = document.createElement('span');
  const cardBio = document.createElement('p');
  const nameText = document.createTextNode(name);
  const bioContent = document.createTextNode(bio);

  card.classList.add('card');
  cardInner.classList.add('card__inner');
  cardFront.classList.add('card__front');
  cardBack.classList.add('card__back');
  cardImg.classList.add('card__img');
  cardName.classList.add('card__name');
  cardBio.classList.add('card__bio');

  gallery.setAttribute('data-simplebar', '');
  cardImg.setAttribute('src', img);
  cardImg.setAttribute('loading', 'lazy');

  cardName.appendChild(nameText);
  cardBio.appendChild(bioContent);
  cardFront.appendChild(cardImg);
  cardBack.appendChild(cardName);
  cardBack.appendChild(cardBio);
  cardInner.appendChild(cardFront);
  cardInner.appendChild(cardBack);
  card.appendChild(cardInner);
  gallery.appendChild(card);
}
