/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */
'use strict';

function createAudioPlayer() {
  const app = document.getElementById('app');
  const audioPlayer = document.createElement('audio');

  audioPlayer.setAttribute('id', 'audioPlayer');
  audioPlayer.setAttribute('src', '../../public/audio/Harry_Potter_bso.mp3');
  audioPlayer.setAttribute('autoplay', 'true');

  app.appendChild(audioPlayer);
}

function changeBSO(song) {
  const songUrl = document.getElementById('audioPlayer').getAttribute('src');

  if (songUrl !== '../../public/audio/Harry_Potter_bso.mp3') {
    const audioPlayer = document.getElementById('audioPlayer');
    audioPlayer.removeAttribute('src');
    audioPlayer.setAttribute('src', '../../public/audio/' + song);

    audioPlayer.play();
  }
}
