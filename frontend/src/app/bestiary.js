/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */
'use strict';

function showBestiary() {
  const mainSection = document.getElementById('main-section');
  mainSection.innerHTML = '';

  const flipbookViewport = document.createElement('div');
  const container = document.createElement('div');
  const flipbook = document.createElement('div');
  const firstPage = document.createElement('div');
  const lastPage = document.createElement('div');

  flipbookViewport.classList.add('flipbook-viewport');
  container.classList.add('container');
  flipbook.classList.add('flipbook');
  firstPage.classList.add('firstPage');
  firstPage.classList.add('hard');
  lastPage.classList.add('lastPage');
  lastPage.classList.add('hard');

  mainSection.appendChild(flipbookViewport);
  flipbookViewport.appendChild(container);
  container.appendChild(flipbook);
  flipbook.appendChild(firstPage);

  fetch('http://localhost:3000/beasts')
      .then((response) => response.json())
      .then((beasts) => {
        for (const beast of beasts) {
          addPage(beast.name, beast.bio, beast.img);
        }

        const firstPages = document.querySelectorAll('.pages')[0];
        const lastPages = document.querySelectorAll('.pages')[document.querySelectorAll('.pages').length - 1];

        firstPages.classList.add('hard');
        lastPages.classList.add('hard');

        flipbook.appendChild(lastPage);
      })
      .then(()=>{

      function loadApp() {
      
          // Create the flipbook
      
          $('.flipbook').turn({
                  // Width
      
                  width:922,
                  
                  // Height
      
                  height:600,
      
                  // Elevation
      
                  elevation: 50,
                  
                  // Enable gradients
      
                  gradients: true,
                  
                  // Auto center this flipbook
      
                  autoCenter: true
      
          });
      }
      
      // Load the HTML4 version if there's not CSS transform
      
      yepnope({
          test : Modernizr.csstransforms,
          yep: ['./src/turnjs/lib/turn.js'],
          nope: ['./src/turnjs/lib/turn.html4.min.js'],
          both: ['./src/turnjs/css/basic.css'],
          complete: loadApp
      })});
}

function addPage(name, bio, img) {
  const flipbook = document.querySelector('.flipbook');
  const page = document.createElement('div');

  page.classList.add('pages');

  flipbook.appendChild(page);
}
